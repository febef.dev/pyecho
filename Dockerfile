
FROM debian:latest

RUN apt update; apt install -qqy python3 python3-pip; apt-get clean --dry-run;

ENV FLASK_ENV=development
RUN mkdir /app
WORKDIR /app

ADD main.py /app
ADD requirements.txt /app
RUN python3 -m pip install -r requirements.txt

EXPOSE 8080

ENTRYPOINT ["python3"]

CMD ["main.py" ]

